#include <stdio.h>
#include <stdlib.h>
#include <math.h>

unsigned int max(unsigned int x, unsigned int y)
{
    if(x >= y)
    {
        return x;
    }
    else
    {
        return y;
    }
}

unsigned int max3(unsigned int x, unsigned int y, unsigned int z)
{
    return max(x, max(y, z));
}

unsigned int histArea(unsigned int hist[],unsigned l, unsigned r)//l is inclusive, r is exclusive
{
    if(l == r)
    {
        return 0;
    }

    unsigned int minimum = 10000;
    unsigned int min_index = 0;
    for(unsigned int i = l; i < r ; i++)
    {
        if(hist[i] < minimum)
        {
            minimum = hist[i];
            min_index = i;
        }
    }
    

    return max3(minimum * (r-l), 
    histArea(hist, l, min_index), histArea(hist, min_index + 1, r));
}


int main(void)
{
    unsigned int n;
    if(scanf("%d",&n) <= 0)
    {
        return 1;
    }


    unsigned int* arr = (unsigned int*) malloc(n * n * sizeof(unsigned int));
    unsigned int* hist = (unsigned int*) calloc(n,  sizeof(unsigned int));
    for(unsigned int i = 0; i < n * n; i++)
    {
        if(scanf("%d",&arr[i]) <= 0)
        {
            return 1;
        }
    }
    
    unsigned int max_sum = 0;

    for(unsigned int i = 0; i < n * n; i++)
    {
        arr[i] = !arr[i];
    }

    for(unsigned int i = 0; i < n; i++)
    {
        
        for(unsigned int j = 0; j < n; j++)
        {
            if(arr[i * n + j] != 0)
            {
                hist[j] += arr[i * n + j];
            }
            else
            {
                hist[j] = 0;
            }
        }
        max_sum = max(max_sum, histArea(hist, 0, n));
    }
    
    printf("%d", max_sum);
    free(arr);
    free(hist);
}