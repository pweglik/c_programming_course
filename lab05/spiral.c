#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    unsigned int n;
    if(scanf("%d",&n) <= 0)
    {
        return 1;
    }
    unsigned int* arr = (unsigned int*) malloc(n * n * sizeof(unsigned int));

    unsigned int i = 1;
    unsigned int layers = 0;
    unsigned int x = 0;
    unsigned int y = 0;

    while(i <= n * n)
    {
        arr[x * n + y] = i;
        if(x == layers)
        {
            if(y != n - 1 - layers)
            {
                y++;
            }
            else
            {
                x++;
            }
        }
        else if(y == n - 1 - layers)
        {
            if(x != n - 1 - layers)
            {
                x++;
            }
            else
            {
                y--;
            }
        }
        else if(x == n - 1 - layers)
        {
            if(y != layers)
            {
                y--;
            }
            else
            {
                x--;
            }
        }
        else if(y == layers)
        {
            if(x != layers + 1)
            {
                x--;
            }
            else
            {
                y++;
                layers++;
            }
        }
        i++;
    }

    for(unsigned int j = 0; j < n; j++)
    {
        for(unsigned int k = 0; k < n; k++)
        {
            printf("%d ", arr[j * n + k]);
        }
        printf("\n");
    }
    
    free(arr);
}