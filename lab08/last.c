#include <stdio.h>
#include <stdlib.h>

int length(long long int i){
    int len = 0;
    while(i != 0){
        i/=10;
        len+=1;
    }
    return len;
}

int main(){
    long long int sum = 0;
    scanf("%lld",&sum);
    
    long long int len = length(sum);

    int i = 1;
    long long int mask = 1;
    while (i < len){
        mask *= 10;
        mask ++;
        i++;
    }

    int* result = malloc(sizeof(int)*(long long unsigned int)len);
    for (int i = 0; i< len; i++){
        while (sum - mask >= 0){
            sum -= mask;
            result[i]++;
            if (result[i] > 9)
            {
                printf("-1"); 
                free(result); 
                return -1;
            }
        }
        mask/=10;
    }

    for(int i = (result[0] == 0) ? 1 : 0 ; i < len; i++){
        printf("%d",result[i]);
    }

    free(result);
}