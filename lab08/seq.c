#include <stdio.h>
#include <stdlib.h>


int main(void)
{
    char string[50];
    char target[50] = "";
    if(scanf("%s", string) <= 0)
    {
        return 1;
    }
    unsigned int i = 0;
    unsigned int target_index = 0;
    char biggest_char;
    unsigned int biggest_index ;

    while(string[i] != '\0' && i < 50)
    {
        biggest_char = 'a';
        biggest_index = i;

        unsigned int j = i;
        while(string[j] != '\0' && j < 50)
        {
            if(string[j] > biggest_char)
            {
                biggest_char = string[j];
                biggest_index = j;
            }

            j++;
        }
        target[target_index] = biggest_char;
        target_index++;

        i = biggest_index + 1;
    }

    printf("%s\n", target);
}