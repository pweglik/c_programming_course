#include <stdio.h>
#include <stdlib.h>


int main(void)
{
    unsigned long long int n;
    if(scanf("%lld", &n) <= 0)
    {
        return 1;
    }
    unsigned long long int sum = 0;
    
    //summing odd numbers
    if(n % 2 == 0)
    {
        unsigned long long int x = n/2;
        sum +=  x * x;
    }
    else
    {
        unsigned long long int x = (n+1)/2;
        sum +=  x * x;
    }

    //summing special even numbers
    if(n % 2 == 0)
    {
        unsigned long long int x = n/4;
        sum +=  x * x;
    }
    else
    {
        unsigned long long int x = (n+1)/4;
        sum +=  x * x;
    }

    //adding rest of divisors of even numbers
    unsigned long long int i = 4;
    unsigned long long int copy;
    while(i <= n)
    {
        copy = i;
        while(copy % 2 == 0)
        {
            copy = copy >> 1;
        }
        sum += copy;
        i += 4;
    }

    printf("%lld\n", sum);
}