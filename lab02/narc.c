#include <stdio.h>
#include <string.h>
#include <math.h>

char digits[] = "0123456789ABCDEF";

int any_to_dec(char number[], int base)
{
    
    int dec_number = 0;
    for(int i = 0; i < strlen(number); i++)
    {
        dec_number += (strchr(digits, number[i]) - digits) * pow(base, strlen(number) - 1 - i);
    }
    return dec_number;
}

int narc_sum(char number[], int m)
{
    int sum = 0;
    for(int i = 0; i < m; i++)
    {
        sum += pow((strchr(digits, number[i]) - digits), m);
    }
    return sum;
}

int main(void)
{
    // printf("%s", dec_to_any(10,2));

    int m;
    if(scanf("%d", &m) <= 0)
    {
        return 1;
    }
    
    int base;
    if(scanf("%d", &base) <= 0)
    {
        return 1;
    }
    
    char number[m+1];
    char check[m+1];

    for(int i = 0; i < m; i++)
    {
        number[i] = '0';
        check[i] = '0';
    }
    number[m] = '\0';
    check[m] = '\0';
    int dec_number, n_sum;
    int flag = 0;

    while(1)
    {
        // printf("%s\n ", number);
        dec_number = any_to_dec(number, base);
        n_sum = narc_sum(number, m);
        if(dec_number == n_sum && number[0] != '0')
        {
            printf("%s ", number);
            flag = 1;
        }

        for(int i = m-1; i > -1; i--)
        {
            if(number[i] != digits[base-1])
            {
                int k = strchr(digits, number[i]) - digits + 1;
                number[i] = digits[k];
                break;        
            }
            else
            {
                number[i] = digits[0];
            }
        }
        if(strcmp(number, check) == 0)
        {
            break;
        }

    }
    if(flag == 0)
    {
        printf("NO");
    }
    
    return 0;
}