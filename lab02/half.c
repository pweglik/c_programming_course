#include <stdio.h>

int main(void)
{
    int n;
    if(scanf("%d", &n) <= 0)
    {
        return 1;
    }
    
    int arr[n];
    for(int i = 0; i < n; i++)
    {
        if(scanf("%d", &arr[i]) <= 0)
        {
            return 1;
        }
    }
    int sum = 0;
    for(int i = 0; i < n; i++)
    {
        sum += arr[i];
    }
    int p = -1;
    int subsum = 0;
    for(int i = 0; i < n; i++)
    {
        subsum += arr[i];
        if( (subsum - arr[i]) * 2 == (sum - arr[i]) )
        {
            p = i;
            break;
        }
    }
    printf("%d", p);
    return 0;
}