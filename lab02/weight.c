#include <stdio.h>
#include <math.h>

int found = 0;

void solve_weight(int sum, int p, int w, int n, int arr[])
{
    if(found == 1)
    {
        return;
    }
    if(sum == w)
    {
        found = 1;
        return;
    }
    if(p > n)
    {
        return;
    }

    solve_weight(sum, p + 1, w, n, arr);

    solve_weight(sum + arr[p], p + 1, w, n, arr);

    solve_weight(sum - arr[p], p + 1, w, n, arr);

    
}

int main(void)
{
    // printf("%s", dec_to_any(10,2));

    int n;
    if(scanf("%d", &n) <= 0)
    {
        return 1;
    }
    

    int w;
    if(scanf("%d", &w) <= 0)
    {
        return 1;
    }
    

    int arr[n];
    int sum = 0;

    for(int i = 0; i < n; i++)
    {
        if(scanf("%d", &arr[i]) <= 0)
        {
            return 1;
        }
        
        sum += arr[i];
    }

    if(sum > w)
    {
        solve_weight(0, 0, w, n, arr);
        if(found == 1)
        {
            printf("YES");
        }
        else
        {
            printf("NO");
        }
    }
    else
    {
        printf("NO");
    }

    
    return 0;
}