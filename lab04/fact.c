#include <stdio.h>
#include <stdlib.h>
#define SIZE 200


int main(void)
{
    int n;
    if(scanf("%d",&n) <= 0)
    {
        return 1;
    }
    int* arr = (int*) calloc(SIZE, sizeof(int));

    arr[0] = 1;

    int i = 2;
    while(i <= n)
    {
        int carry = 0;
        int j = 0;
        while(j < SIZE)
        {
            int temp_value = arr[j] * i + carry;
            arr[j] = temp_value % 10;
            carry = temp_value / 10;
            j++;
        }


        i++;
    }

    int j = SIZE;
    while(j >= 0)
    {
        if(arr[j] > 0)
        {
            break;
        }
        j--;
    }
    while(j >= 0)
    {
        printf("%d", arr[j]);
        j--;
    }
    
    free(arr);
}