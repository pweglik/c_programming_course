#include <stdio.h>
#include <stdlib.h>


int main(void)
{
    // some shit, replace later
    unsigned int n;
    if(scanf("%d",&n) <= 0)
    {
        return 1;
    }
    unsigned int r;
    if(scanf("%d",&r) <= 0)
    {
        return 1;
    }
    
    int* F = (int*) malloc(n*n * sizeof(int));
    int* W = (int*) calloc(n*n, sizeof(int));

    for(int i = 0; i < n * n; i++)
    {
        if(scanf("%d", &F[i]) <= 0)
        {
            return 1;
        }
    }

    for(unsigned int i = 0; i < n; i++)
    {
        for(unsigned int j = 0; j < n; j++)
        {
            if(F[i * n + j] == 1)
            {
                unsigned int k;
                unsigned int k_limit = i + r;
                
                if(r > i)
                {
                    k = 0;
                }
                else
                {
                    k = i - r;
                }

                if(k_limit > n - 1)
                {
                    k_limit = n - 1;
                }

                for(; k <= k_limit; k++)
                {
                    unsigned int l;
                    unsigned int l_limit = j + r;
                    if(r > j)
                    {
                        l = 0;
                    }
                    else
                    {
                        l = j - r;
                    }
                    if(l_limit > n - 1)
                    {
                        l_limit = n - 1;
                    }
                    
                    for(; l <= l_limit; l++)
                    {
                        W[k * n + l]++;
                    }

                }
            }
        }
    }

    for(unsigned int i = 0; i < n; i++)
    {
        for(unsigned int j = 0; j < n; j++)
        {
            printf("%d ", W[i * n + j]);
        }
        printf("\n");
    }

    free(F);
    free(W);
}