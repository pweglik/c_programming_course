#include <stdio.h>
#include <stdlib.h>



unsigned int merge(int arr[], int old_output[], int new_output[], unsigned int start_index, unsigned int n, unsigned int size)
{
    unsigned int i = 0;
    unsigned int j = 0;
    unsigned int skipped = 0;
    while(1)
    {
        if((arr[start_index + i] <= old_output[j] || j >= size) && i < n)
        {
            if(i + j == 0 || new_output[i + j - 1 - skipped] != arr[start_index + i])
            {
                new_output[i + j - skipped] = arr[start_index + i];
            }
            else
            {
                skipped++;
            }
            i++;
        }
        else if((arr[start_index + i] > old_output[j] || i >= n) && j < size)
        {
            if(i + j == 0 || new_output[i + j - 1 - skipped] != old_output[j])
            {
                new_output[i + j - skipped] = old_output[j];
            }
            else
            {
                skipped++;
            }
            j++;
        }
        else 
        {
            break;
        }
    }
    return i + j - skipped;   
}

int main(void)
{
    unsigned int n;
    if(scanf("%d",&n) <= 0)
    {
        return 1;
    }
    
    int* arr = (int*) malloc(n*n * sizeof(int));
    int* output1 = (int*) calloc(n*n, sizeof(int));
    int* output2 = (int*) calloc(n*n, sizeof(int));

    for(int i = 0; i < n * n; i++)
    {
        if(scanf("%d", &arr[i]) <= 0)
        {
            return 1;
        }
    }

    unsigned int size = 0;

    int end = 0;
    for(unsigned int i = 0; i < n; i++)
    {
        if(i % 2 == 0)
        {
            size = merge(arr, output1, output2, n * i, n, size);
            end = 2;
        }
        else
        {
            size = merge(arr, output2, output1, n * i, n, size);
            end = 1;
        }
    }
    int* final;
    if(end == 1)
    {
        final = output1;
        free(output2);
    }
    else
    {
        final = output2;
        free(output1);
    }
    
    for(int i = 0; i < size; i++)
    {
        printf("%d ", final[i]);
    }

    free(arr);
    free(final);
}