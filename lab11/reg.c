#include <stdio.h>

int is_power_of_2(int num)
{
    while (num % 2 == 0)
    {
        num /= 2;
    }
    if (num > 1) return 0;
    return 1;
}

int main()
{
    // take input
    long s, t;
    scanf("%ld %ld", &s, &t);
    long copy_t = t;

    /*Aby dało sie przedstawić liczbę t za pomocą tych operacji musi być ona postaci s^x * 2 ^ y, przy czym x=0 lub x=1 lub x = 2 ^ z. Wyjątek: t=1*/
    if (t == 1)
    {
        if (s == 1)
        {
            return 0;
        }
        else
        {
            printf("/");
            return 0;
        }
    }

    // t = s^x * 2^ y - calculate x and y
    int number_of_s = 0;
    int number_of_2 = 0;
    while (copy_t % s == 0)
    {
        number_of_s ++;
        copy_t /= s;
    }
    while (copy_t % 2 == 0)
    {
        number_of_2 ++;
        copy_t /= 2;
    }

    char result[100];
    int index = 0;
    int temp, i;

    if (number_of_s == 0 && s != 1)
    {
        result[index] = '/';
        result[index + 1] = '+';
        index += 2;
        number_of_s = number_of_2;
        number_of_2 = 0;
        s = 2;
    }

    // check if t can be represented
    if (copy_t > 1)
    {
        printf("NO\n");
        return 0;
    }
    if (number_of_s != 0 && number_of_s != 1 && is_power_of_2(number_of_s) == 0)
    {
        if (s == 2)
        {
            while (is_power_of_2(number_of_s) == 0)
            {
                number_of_s --;
                number_of_2 ++;
            }
        }
        else
        {
            printf("NO\n");
            return 0;
        }
    }

    while (number_of_s > 1)
    {
        temp = number_of_2 / number_of_s;
        i = 0;
        while (i < temp)
        {
            result[index] = '+';
            index ++;
            i ++;
        }
        number_of_2 -= number_of_s * temp;
        result[index] = '*';
        index ++;
        number_of_s /= 2;
    }
    while (number_of_2 > 0)
    {
        result[index] = '+';
        index ++;
        number_of_2 --;
    }
    for (int j=0; j<index; j++)
    {
        printf("%c", result[j]);
    }
    printf("\n");
    return 0;
}