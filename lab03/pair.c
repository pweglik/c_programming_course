#include <stdio.h>
#include <stdlib.h>

int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}



int main(void)
{
    int n,k;
    scanf("%d %d",&n,&k);
    int* arr = (int*) calloc(n, sizeof(int));
    for(int i=0;i<n;i++){
        scanf("%d",&arr[i]);
    }

    qsort(arr, n, sizeof(int), cmpfunc);
    
    int count = 0;

    int i = 0;
    int stack = 1;
    while(i < n)
    {
        if(i == 0 || arr[i] - arr[i - 1] > k)
        {
            stack = 1;
            i++;
            while(i < n && arr[i] == arr[i-1]){
                stack += 1;
                i++;
            }
            if(i==n){
                break;
            }
            if(arr[i] - arr[i-1] <= k){
                count += stack;
            }
        }
        else
        {
            count += 1;
            i++;
            while(i < n && arr[i] == arr[i-1]){
                count++;
                i++;
            }
            if(i == n)
            {
                break;
            }
        }
    }
    printf("%d", count);
}