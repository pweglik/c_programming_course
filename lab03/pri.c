#include <stdio.h>
#include <math.h>

int is_prime(int n)
{
    if (n < 2)
    {
        return 0;
    }
    if (n == 2 || n == 3)
    {
        return 1;
    }
    if (n % 2 == 0 || n % 3 == 0)
    {
        return 0;
    }
    int i = 6;
    while (i - 1 <= sqrt(n))
    {
        if (n % (i + 1) == 0 || n % (i - 1) == 0)
        {
            return 0;
        }
        i += 6;
    }
    return 1;
}

int step(int n)
{
    if(n == 1)
    {
        return 1;
    }
    else if(n == 4)
    {
        return 0;
    }
    else
    {
        int k = 0;
        while(n > 0)
        {
            k += pow(n%10, 2);
            n = (n - n % 10)/10;
        }
        return step(k);
    }
}

int main(void)
{
    int l;
    if(scanf("%d", &l) <= 0)
    {
        return 1;
    }
    int u;
    if(scanf("%d", &u) <= 0)
    {
        return 1;
    }
    int k;
    if(scanf("%d", &k) <= 0)
    {
        return 1;
    }
   
    int i = l;
    int to_ret = -1;
    while(i <= u)
    {
        if( step(i) == 1 && is_prime(i) == 1)
        {
            k = k - 1;
            if(k == 0)
            {
                to_ret = i;
                break;
            }
        }
        i++;
    }

    
    printf("%d", to_ret);
}