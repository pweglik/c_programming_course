#include <stdio.h>
#include <limits.h>
#include <math.h>


int main(void)
{
    int n;
    if(scanf("%d", &n) <= 0)
    {
        return 1;
    }
    int k;
    if(scanf("%d", &k) <= 0)
    {
        return 1;
    }
    
    int arr[n];
    for(int i = 0; i < n; i++)
    {
        if(scanf("%d", &arr[i]) <= 0)
        {
            return 1;
        }
    }
    
    int biggest_index = -1;
    int biggest = INT_MIN;

    for(int j = 0; j < k; j++)
    {
        biggest_index = -1;
        biggest = INT_MIN;

        for(int i = 0; i < n; i++)
        {
            if(arr[i] > biggest)
            {
                biggest = arr[i];
                biggest_index = i;
            }
        }
        arr[biggest_index] = floor(arr[biggest_index]/2);
    }
    int sum = 0;
    for(int i = 0; i < n; i++)
    {
        sum += arr[i];
    }
    printf("%d", sum);
}