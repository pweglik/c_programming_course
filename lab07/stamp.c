#include <stdio.h>
#include <stdlib.h>


int main(void)
{
    unsigned int n;
    if(scanf("%d",&n) <= 0)
    {
        return 1;
    }

    unsigned int k;
    if(scanf("%d",&k) <= 0)
    {
        return 1;
    }

    unsigned int l;
    if(scanf("%d",&l) <= 0)
    {
        return 1;
    }

    int* T = (int*) calloc(n * n,  sizeof(int));
    int* P = (int*) calloc(k * l,  sizeof(int));
    
    
    for(unsigned int i = 0; i < n; i++)
    {
        for(unsigned int j = 0; j < n; j++)
        {
            if(scanf("%d", &T[i * n + j]) <= 0)
            {
                return 1;
            }
        }
    }
    for(unsigned int i = 0; i < k; i++)
    {
        for(unsigned int j = 0; j < l; j++)
        {
            if(scanf("%d", &P[i * l + j]) <= 0)
            {
                return 1;
            }
        }
    }

    int max_sum = 0;
    for(unsigned int ii = 0; ii < k; ii++)
    {
        for(unsigned int jj = 0; jj < l; jj++)
        {
            if(P[ii * l + jj] == 1)
            {
                max_sum += T[ (ii) * n + (jj) ];
            }
        }   
    }
    
    for(unsigned int i = 0; i + k <= n; i++)
    {
        for(unsigned int j = 0; j + l <= n; j++)
        {
            int cur_sum = 0;
            for(unsigned int ii = 0; ii < k; ii++)
            {
                for(unsigned int jj = 0; jj < l; jj++)
                {
                    if(P[ii * l + jj] == 1)
                    {
                        cur_sum += T[ (i + ii) * n + (j + jj) ];
                    }
                }   
            }
            if(cur_sum > max_sum)
            {
                max_sum = cur_sum;
            }
        }
    }

    printf("%d", max_sum);
    free(T);
    free(P);
}