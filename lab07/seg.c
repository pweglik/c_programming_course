#include <stdio.h>
#include <stdlib.h>



int maxFromPoint(unsigned int i, unsigned int j, int **arr, unsigned int n, unsigned int k)
{
    int sum = 0;
    int cur_sum[8] = {0,0,0,0,0,0,0,0};
    for(unsigned int l = 0; l < k; l++)
    {
        cur_sum[0] += arr[i][(j + l + n)%n];
        cur_sum[1] += arr[(i + l + n)%n][(j + l + n)%n];
        cur_sum[2] += arr[(i + l + n)%n][j];
        cur_sum[3] += arr[(i + l + n)%n][(j - l + n)%n];
        cur_sum[4] += arr[i][(j - l + n)%n];
        cur_sum[5] += arr[(i - l + n)%n][(j - l + n)%n];
        cur_sum[6] += arr[(i - l + n)%n][j];
        cur_sum[7] += arr[(i - l + n)%n][(j + l + n)%n];
    }
    for(unsigned int l = 0; l < 8; l++)
    {
        if(cur_sum[l] > sum)
        {
            sum = cur_sum[l];
        }
    }
    return sum;
}

int main(void)
{
    unsigned int n;
    if(scanf("%d",&n) <= 0)
    {
        return 1;
    }

    unsigned int k;
    if(scanf("%d",&k) <= 0)
    {
        return 1;
    }

    int** T = (int**) malloc(n * sizeof(int*));
    
    for(unsigned int i = 0; i < n; i++)
    {
        T[i] = (int*) malloc(n * sizeof(int));
        for(unsigned int j = 0; j < n; j++)
        {
            if(scanf("%d", &T[i][j]) <= 0)
            {
                return 1;
            }
        }
    }
    int max_sum = 0;
    int cur_sum = 0;
    for(unsigned int i = 0; i < n; i++)
    {
        for(unsigned int j = 0; j < n; j++)
        {
            cur_sum = maxFromPoint(i, j, T, n , k);
            if(cur_sum > max_sum)
            {
                max_sum = cur_sum;
            }
        }
    }
  

    printf("%d", max_sum);
    free(T);

}