#include <stdio.h>
#include <stdlib.h>
#include <math.h>


void decToBin(int dec, char bin[], unsigned int n)
{
    for(unsigned i = 0; i < n; i++)
    {
        bin[i] = '0';
    }
    unsigned int i = 0;
    while(dec > 0)
    {
        
        if(dec % 2 == 1)
        {
            bin[n - 1 - i] = '1';
            dec = (dec - 1) / 2;
        }
        else
        {
            dec = dec / 2;
        }
        i++;
    }
}

int isBinCorrect(int num_bin, unsigned int n) // 1 when is correct, 0 when is not correct, -1 when exceeded range n
{
    int counter = 0;
    while(num_bin > 0)
    {
        if(counter == n)
        {
            return -1;
        }
        if(num_bin % 2 == 1)
        {
            num_bin = (num_bin - 1) / 2;
            if(num_bin % 2 == 1)
            {
                return 0;
            }
        }
        else
        {
            num_bin = num_bin / 2;
        }
        counter++;
    }
    return 1;
}

void decToSpecialBin(char bin[], unsigned int n, unsigned int k)
{
    int counter = 0;
    int correct_counter = 0;
    int check = 0;
    
    while(1 == 1)
    {
        if(correct_counter == k)
        {
            decToBin(counter-1, bin, n);
            break;
        }
        check = isBinCorrect(counter, n);
        if(check == 1)
        {
            correct_counter++;
        }
        else if (check == -1)
        {
            bin[0] = '-';
            bin[1] = '1';
            bin[2] = '\0';
            break;
        }
        counter++;
    } 
}

int main(void)
{
    unsigned int n;
    if(scanf("%d",&n) <= 0)
    {
        return 1;
    }

    unsigned int k;
    if(scanf("%d",&k) <= 0)
    {
        return 1;
    }
    char* bin = (char*) calloc(n,  sizeof(char));
    
    decToSpecialBin(bin, n, k);

    printf("%s", bin);
    free(bin);
}