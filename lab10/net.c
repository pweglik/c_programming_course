#include <stdio.h>
#include <stdlib.h>


int binEq(int* bin1, int* bin2, unsigned n) // 1 for equal, 0 for not equal
{
    for(unsigned int i = 0; i < n; i++)
    {
        if(bin1[n - 1 - i] != bin2[n - 1 - i])
        {
            return 0;
        }
    }
    return 1;
}

int* nextBinary(int* binary, unsigned n)
{
    for(unsigned int i = 0; i < n; i++)
    {
        if(binary[n - 1 - i] == 1)
        {
            binary[n - 1 - i] = 0;
        }
        else
        {
            binary[n - 1 - i] = 1;
            break;
        }
        
    }
    return binary;
}

int calculateOutput(int* inputs, int** inOrderGates, unsigned int m)
{
    int* gateOutput = (int*) calloc(m, sizeof(int));
    for(unsigned int i = 0; i < m; i++)
    {
        int input1, input2;
        if(inOrderGates[i][0] < 0)
        {
            input1 = inputs[ (inOrderGates[i][0] * -1) - 1];
        }
        else
        {
            input1 = gateOutput[ inOrderGates[i][0] - 1];
        }
        if(inOrderGates[i][1] < 0)
        {
            input2 = inputs[ (inOrderGates[i][1] * -1) - 1];
        }
        else
        {
            input2 = gateOutput[ inOrderGates[i][1] - 1];
        }
        
        gateOutput[ inOrderGates[i][2] ] = input1 ^ input2;
    }

    int output = gateOutput[ inOrderGates[m-1][2] ];
    free(gateOutput);
    return output;
}

int main(void)
{
    //input and some declarations
    unsigned int n;
    unsigned int m;
    unsigned int x;
    if(scanf("%u %u %u",&n, &m, &x) <= 0)
    {
        return 1;
    }

    int** gates = (int**) malloc(m * sizeof(int*));
    int** inOrderGates = (int**) malloc(m * sizeof(int*)); // array to store xor gates in right order topologically sorted ;)
    
    for(unsigned int i = 0; i < m; i++)
    {
        gates[i] = (int*) malloc(2 * sizeof(int));
        inOrderGates[i] = (int*) malloc(3 * sizeof(int)); // extra space for previous index

        for(unsigned int j = 0; j < 2; j++)
        {
            if(scanf("%d", &gates[i][j]) <= 0)
            {
                return 1;
            }
        }
    }

    char a[150];
    char b[150];
    int* a_arr = (int*) malloc(n * sizeof(int));
    int* b_arr = (int*) malloc(n * sizeof(int));
    if(scanf("%s",a) <= 0)
    {
        return 1;
    }
    if(scanf("%s",b) <= 0)
    {
        return 1;
    }

    // conversion to 0/1 ints from 0/1 chars
    for(unsigned int i = 0; i < n; i++)
    { 
        a_arr[i] = (int) (a[i]) - 48; 
        b_arr[i] = (int) (b[i]) - 48; 
    }

    // transform T into inorder T in O(m^2)
    int* check = (int*) calloc(m, sizeof(int));
    unsigned int inOrderIndex = 0;
    for(unsigned int j = 0; j < m; j++)
    {
        for(unsigned int i = 0; i < m; i++)
        {
            if(check[i] == 0)
            {
                int flag = 0;
                if(gates[i][0] < 0 && gates[i][1] < 0)
                {
                    flag = 1;
                }
                else if(gates[i][0] < 0 && check[ gates[i][1]-1 ] == 1)
                {
                    flag = 1;
                }
                else if(check[ gates[i][0]-1 ] == 1 && gates[i][1] < 0)
                {
                    flag = 1;
                }
                else if(check[ gates[i][0]-1 ] == 1 && check[ gates[i][1]-1 ] == 1)
                {
                    flag = 1;
                }

                if(flag == 1)
                {
                    check[i] = 1;
                    inOrderGates[inOrderIndex][0] = gates[i][0];
                    inOrderGates[inOrderIndex][1] = gates[i][1];
                    inOrderGates[inOrderIndex][2] = (int) i;
                    inOrderIndex++;
                }

            }
            
        }
    }

    
    // calculate output for every set of inputs between a and b
    long long sum = 0;
    while(binEq(a_arr, b_arr, n) == 0)
    {
        sum += calculateOutput(a_arr, inOrderGates, m);;
        a_arr = nextBinary(a_arr, n);
    }

    sum += calculateOutput(a_arr, inOrderGates, m);;

    printf("%lld", sum);


    free(a_arr);
    free(b_arr);
    free(check);
    free(gates);
    free(inOrderGates);
}