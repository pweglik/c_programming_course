#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    unsigned int n;
    if(scanf("%u",&n) <= 0)
    {
        return 1;
    }

    int g;
    if(scanf("%d",&g) <= 0)
    {
        return 1;
    }

    int* T = (int*) malloc(n * sizeof(int));
    
    for(unsigned int i = 0; i < n; i++)
    {
        if(scanf("%d", &T[i]) <= 0)
        {
            return 1;
        }
    }

    int min = (int) n;
    for(int i = 0; i < 32; i++)
    {
        int temp = -1;
        if( ((g >> i) & 1) == 1)
        {
            temp = 0;
            for(unsigned int j = 0; j < n; j++)
            {
                int flag = 0;
                for(int k = 0; k < 32; k++)
                {
                    if( ((g >> k) & 1) == 0 && ((T[j] >> k) & 1) == 1)
                    {
                        flag = 1;
                        break;
                    }
                }
                if( ((T[j] >> i) & 1) == 1 && flag == 0)
                {
                    temp++;
                }
            }
        }

        if(temp < min && temp != -1)
        {
            min = temp;
        }
        
    }


    printf("%d", min);

    free(T);
}