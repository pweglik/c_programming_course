#include <stdio.h>
#include <math.h>
#include <string.h>


int compare_strings(char str1[], char str2[]) // 0 if equal, 1 if str1 bigger, 2 if str2 bigger
{
    unsigned int n1 = (unsigned int) strlen(str1);
    unsigned int n2 = (unsigned int) strlen(str2);
    int flag = 0;

    int minlen = (int) fmin(n1, n2);
    for(unsigned int i = 0; i < minlen; i++)
    {
        if(str1[i] > str2[i])
        {
            flag = 1;
            break;
        }
        else if(str1[i] < str2[i])
        {
            flag = 2;
            break;
        }
    }
    if(flag == 0)
    {
        if(n1 > n2)
        {
            flag = 1;
        }
        else if(n1 < n2)
        {
            flag = 2;
        }
    }
    return flag;
}

int is_prime(int n)// 1 if n is prime
{
    if (n < 2)
    {
        return 0;
    }
    if (n == 2 || n == 3)
    {
        return 1;
    }
    if (n % 2 == 0 || n % 3 == 0)
    {
        return 0;
    }
    int i = 6;
    while (i - 1 <= sqrt(n))
    {
        if (n % (i + 1) == 0 || n % (i - 1) == 0)
        {
            return 0;
        }
        i += 6;
    }
    return 1;
}

int main(void)
{
    char pattern[51];
    char minimal[51];
    char temp[51] = "";

    if(scanf("%s",pattern) <= 0)
    {
        return 1;
    }

    strcpy(minimal, pattern);
    unsigned int n = (unsigned int) strlen(pattern);
    
    for(unsigned int o = 0; o < n; o++)
    {
        for(unsigned int p = 0; p < n; p++)
        {
            if(is_prime((int)p) == 1)
            {
                for(unsigned int k = 0; k < n; k++)
                {
                    temp[k] = pattern[(o + k * p) % n];
                }
                if(compare_strings(temp, minimal) == 2)
                {
                    strcpy(minimal, temp);
                }
            }
        }
    }

    printf("%s", minimal);
}