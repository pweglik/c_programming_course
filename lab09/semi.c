#include <stdio.h>
#include <math.h>

int main(void)
{
    int n;
    if(scanf("%d",&n) <= 0)
    {
        return 1;
    }

    int m;
    if(scanf("%d",&m) <= 0)
    {
        return 1;
    }

    int sum = 0;
    int cur = 0;
    int max = (int) pow(2, n);
    for(int i = 0; i < max; i++)
    {
        if(i % m != 0)
        {
            for(int j = 0; j < n; j++)
            {   
                cur = (i ^ (1 << j));
                if( cur % m == 0 && cur != 0)
                {
                    sum++;
                    break;
                }
            }
        }
    }

    printf("%d", sum);
}