#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 50

int romanToDecDigit(char c)
{
    int num = 0;
    switch(c)
    {
        case 'I':
            num = 1;
            break;
        case 'V':
            num = 5;
            break;
        case 'X':
            num = 10;
            break;
        case 'L':
            num = 50;
            break;
        case 'C':
            num = 100;
            break;
        case 'D':
            num = 500;
            break;
        case 'M':
            num = 1000;
            break;
    }
    return num;
}

char decToRomanDigit(int i)
{
    char c = ' ';
    switch(i)
    {
        case 1:
            c = 'I';
            break;
        case 5:
            c = 'V';
            break;
        case 10:
            c = 'X';
            break;
        case 50:
            c = 'L';
            break;
        case 100:
            c = 'C';
            break;
        case 500:
            c = 'D';
            break;
        case 1000:
            c = 'M';
            break;
    }
    return c;
}

int romanToDec(char num[])
{
    int decNum = 0;
    unsigned int i = 0;
    while(num[i] != '\0')
    {
        if(num[i] == 'I' && (num[i+1] == 'V' || num[i+1] == 'X') )
        {
            if(num[i+1] == 'V')
            {
                decNum += 4;
                i += 2;
            }
            else if(num[i+1] == 'X')
            {
                decNum += 9;
                i += 2;
            }
        }
        else if(num[i] == 'X' && (num[i+1] == 'L' || num[i+1] == 'C') )
        {
            if(num[i+1] == 'L')
            {
                decNum += 40;
                i += 2;
            }
            else if(num[i+1] == 'C')
            {
                decNum += 90;
                i += 2;
            }
        }
        else if(num[i] == 'C' && (num[i+1] == 'D' || num[i+1] == 'M') )
        {
            if(num[i+1] == 'D')
            {
                decNum += 400;
                i += 2;
            }
            else if(num[i+1] == 'M')
            {
                decNum += 900;
                i += 2;
            }
        }
        else
        {
            decNum += romanToDecDigit(num[i]);
            i++;
        }
    }
    return decNum;
}

char *decToRoman(int num, char romanNum[])
{
    int len = 0;
    while(num > 0)
    {
        if(num >= 1000)
        {
            romanNum[len] = 'M';
            num -= 1000;

        }
        else if(num >= 900)
        {
            romanNum[len] = 'C';
            len++;
            romanNum[len] = 'M';
            num -= 900;
        }
        else if(num >= 500)
        {
            romanNum[len] = 'D';
            num -= 500;
        }
        else if(num >= 400)
        {
            romanNum[len] = 'C';
            len++;
            romanNum[len] = 'D';
            num -= 400;
        }
        else if(num >= 100)
        {
            romanNum[len] = 'C';
            num -= 100;
        }
        else if(num >= 90)
        {
            romanNum[len] = 'X';
            len++;
            romanNum[len] = 'C';
            num -= 90;
        }
        else if(num >= 50)
        {
            romanNum[len] = 'L';
            num -= 50;
        }
        else if(num >= 40)
        {
            romanNum[len] = 'X';
            len++;
            romanNum[len] = 'L';
            num -= 40;
        }
        else if(num >= 10)
        {
            romanNum[len] = 'X';
            num -= 10;
        }
        else if(num >= 9)
        {
            romanNum[len] = 'I';
            len++;
            romanNum[len] = 'X';
            num -= 9;
        }
        else if(num >= 5)
        {
            romanNum[len] = 'V';
            num -= 5;
        }
        else if(num >= 4)
        {
            romanNum[len] = 'I';
            len++;
            romanNum[len] = 'V';
            num -= 9;
        }
        else if(num >= 1)
        {
            romanNum[len] = 'I';
            num -= 1;
        }
        len++;
    }
    romanNum[len] = '\0';
    return &(romanNum[0]);
}

char *addTwoRomanNumbers(char num1[], char num2[], char sum[])
{
    int dec1 = romanToDec(num1);
    int dec2 = romanToDec(num2);
    int sumDec = dec1 + dec2;
    sum = decToRoman(sumDec, sum);
    return &(sum[0]);
}   

int main(void)
{
    char num1[SIZE];
    char num2[SIZE];
    if(scanf("%s",num1) <= 0)
    {
        return 1;
    }
    if(scanf("%s",num2) <= 0)
    {
        return 1;
    }
    char sumRoman[SIZE];
    strcpy(sumRoman, addTwoRomanNumbers(num1, num2, sumRoman));
    printf("%s", sumRoman);
  
}