#include <stdio.h>
#include <stdlib.h>

#define BOARD_SIZE 200

struct Rect
{
    int x1;
    int y1;
    int x2;
    int y2;
};

void flipRectInBoard(struct Rect rect, int board[])
{
    int x1 = rect.x1 + BOARD_SIZE/2 ;
    int y1 = rect.y1 * -1 + BOARD_SIZE/2 - 1;
    int x2 = rect.x2 + BOARD_SIZE/2;
    int y2 = rect.y2 * -1 + BOARD_SIZE/2 - 1;
    for(int i = y1; i > y2; i--)
    {
        for(int j = x1; j < x2; j++)
        {
            board[i*BOARD_SIZE + j] = 1 - board[i * BOARD_SIZE + j];
        }
    }
}

int main(void)
{
    unsigned int n;
    if(scanf("%d",&n) <= 0)
    {
        return 1;
    }

    int* board = (int*) calloc(BOARD_SIZE * BOARD_SIZE,  sizeof(int));
    struct Rect* rects = (struct Rect*) malloc(n * sizeof(struct Rect));
    
    for(unsigned int i = 0; i < n; i++)
    {
        if(scanf("%d",&rects[i].x1) <= 0)
        {
            return 1;
        }
        if(scanf("%d",&rects[i].y1) <= 0)
        {
            return 1;
        }
        if(scanf("%d",&rects[i].x2) <= 0)
        {
            return 1;
        }
        if(scanf("%d",&rects[i].y2) <= 0)
        {
            return 1;
        }
    }

    for(int i = 0; i < n; i++)
    {
        flipRectInBoard(rects[i], board);
    }

    int count = 0;
    for(int i = 0; i < BOARD_SIZE; i++)
    {
        for(int j = 0; j < BOARD_SIZE; j++)
        {
           count += board[i*BOARD_SIZE + j];
        }
    }
    printf("%d", count);

    free(rects);
    free(board);
}