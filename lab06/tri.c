#include <stdio.h>
#include <stdlib.h>


int main(void)
{
    unsigned int p;
    if(scanf("%d",&p) <= 0)
    {
        return 1;
    }

    unsigned int n;
    if(scanf("%d",&n) <= 0)
    {
        return 1;
    }

    int* arr = (int*) calloc(p * p,  sizeof(int));
    
    for(unsigned int i = 0; i < n; i++)
    {
        unsigned int a,b;
        if(scanf("%d",&a) <= 0)
        {
            return 1;
        }
        if(scanf("%d",&b) <= 0)
        {
            return 1;
        }
        arr[(a-1)* p + b-1] = 1;
    }


    int count = 0;
    for(unsigned int i = 0; i < p; i++)
    {
        for(unsigned int j = i + 1; j < p; j++)
        {
            for(unsigned int k = j + 1; k < p; k++)
            {
                if(arr[i * p + j] == 1 && arr[i * p + k] == 1 &&
                arr[j * p + k] == 1)
                {
                    count += 1;
                }
                else if(arr[i * p + j] == 0 && arr[i * p + k] == 0 &&
                arr[j * p + k] == 0)
                {
                    count += 1;
                }
            }
        }
    }

    printf("%d",count);
    free(arr);
}