#include <stdio.h>
#include <math.h>

int is_prime(int n)
{
    if (n < 2)
    {
        return 0;
    }
    if (n == 2 || n == 3)
    {
        return 1;
    }
    if (n % 2 == 0 || n % 3 == 0)
    {
        return 0;
    }
    int i = 6;
    while (i - 1 <= sqrt(n))
    {
        if (n % (i + 1) == 0 || n % (i - 1) == 0)
        {
            return 0;
        }
        i += 6;
    }
    return 1;
}

int in_order(int n)
{
    int max = n%10;
    n = (int) n/ 10;

    while(n % 10 <= max && n != 0)
    {
        max = n % 10;
        n = (int) n/ 10;
    }
    if(n == 0)
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}

int main(void)
{
    int n;
    scanf("%d",&n);
    for(int i = 2; i < n; i++)
    {
        if(in_order(i) == 1)
        {
            if(is_prime(i) == 1)
            {
                printf("%d\n", i);
            }
        }
    }
    return 0;

}