#include <stdio.h>
#include <math.h>

int is_prime(int n)
{
    if (n < 2)
    {
        return 0;
    }
    if (n == 2 || n == 3)
    {
        return 1;
    }
    if (n % 2 == 0 || n % 3 == 0)
    {
        return 0;
    }
    int i = 6;
    while (i - 1 <= sqrt(n))
    {
        if (n % (i + 1) == 0 || n % (i - 1) == 0)
        {
            return 0;
        }
        i += 6;
    }
    return 1;
}

int main(void)
{
    int n,m;
    scanf("%d",&n);
    scanf("%d",&m);

    int p = 2;
    int control = 6;
    int count = 0;
    int numbers[52];
    for(int i = 0; i < 51; i++)
    {
        numbers[i] = 0;
    }

    while(control < n)
    {
        p++;
        control = (pow(2, p) - 1) * pow(2, p-1);
    }
    while(control <= m)
    {
        if(is_prime(p) == 1)
        {
            if(is_prime(pow(2, p)-1))
            {
                numbers[count] = control;
                count++;
            }
        }
        p++;
        control = (pow(2, p) - 1) * pow(2, p-1);
    }

    printf("%d\n", count);

    for(int i = 0; i < 51; i++)
    {
        if(numbers[i] != 0)
        {
            printf("%d ", numbers[i]);
        }
        else
        {
            break;
        }
    }
    
    
    return 0;
}