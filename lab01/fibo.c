#include <stdio.h>
#include <math.h>

int main(void)
{
    int n;
    scanf("%d",&n);
    int f1=0,f2=1;

    while (f1*f2 <= n)
    {
        if(f1*f2 == n)
        {
            printf("YES");
            break;
        }
        f2 = f2 + f1;
        f1 = f2 - f1;
    }
    if(f1*f2 > n)
    {
        printf("NO");
    }
    return 0;

}